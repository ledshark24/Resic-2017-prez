.PHONY: clean

all: presentation.html reveal.js/index.html

# Construire la présentation html à partir du fichier source presenation.md
# --incremental pour faire l'animation des listes

presentation.html: presentation.md
	pandoc -t revealjs \
		--toc --toc-depth=1 \
		--standalone \
		--self-contained \
		--slide-level=2 \
		--variable transition="convex" \
		--variable theme="simple" \
		--variable hlss="zenburn" \
		--variable parallaxBackgroundImage=img/fond.png \
		--variable parallaxBackgroundSize="2880px 1800px" \
 		--variable width=1440 \
		--variable height=940 \
		presentation.md -o presentation.html

reveal.js/index.html:
	curl -L https://github.com/hakimel/reveal.js/archive/3.4.1.tar.gz -o reveal.js.tar.gz
	tar zxf reveal.js.tar.gz
	mv reveal.js-3.4.1 reveal.js
	rm reveal.js.tar.gz

plan.md:  presentation.md
	grep "^#" presentation.md > plan.md

clean:
	rm presentation.html
