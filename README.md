# Petit Précis d'hygiène et d'autodéfense numérique

## TL;PL

Présentation donnée lors des RESIC 2017 par le groupe sysinfo.

lancez le fichier `presentation.html` dans votre navigateur.

## Objectifs

à la fin de cette session vous serez capables de

- savoir ce qu'est un GAFAM
- avoir une hygiène numérique
- choisir et retenir un bon mot de passe
- savoir où mettre les clics
- ne pas confondre vie privée et pédo nazi terrorisme
- envisager de chiffrer vos communications

## Moyens pédagogiques (ou pas)

- plusieurs animateurs
- un support de présentation ci inclus à télécharger et compiler soi-même
- vous

## Comment obtenir la présentation

### Prérequis

* [pandoc](http://pandoc.org)
* curl
* make

donc dans toutes les bonnes distributions

`sudo apt-get install pandoc curl make`

### Génération

c'est pas compliqué,

	cd RESIC-2017-prez
	make clean
	make

Attention ça va télécharger reveal.js depuis github et le dé-tar dans le dossier de travail
