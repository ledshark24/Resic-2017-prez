% Hygiène numérique & autodéfense
% RESIC 2017
% [Équipe sysinfo](mailto:sysinfo@isf-france.org)

# Introduction

## Définitions {data-background-image=img/book-alex-read-98811.jpg}

------------------

### Hygiène

> Ensemble des principes, des pratiques individuelles ou collectives visant à la
> conservation de la santé, au fonctionnement normal de l'organisme [...].
> Larousse

. . .

**pratiques, usages individuels et collectifs visant à garantir la vie privée
et la sécurité de chacun**


<div class="notes">
Définition de l'hygiène (au sens Hygiène & sécurité) passer rapidement
</div>

----------------

### Numérique

> Se dit de la représentation d'informations ou de grandeurs physiques au moyen
> de caractères, tels que des chiffres, ou au moyen de signaux à valeurs
> discrètes.


. . .

> Se dit des systèmes, dispositifs ou procédés employant ce mode de
> représentation discrète, par opposition à analogique. Larousse

<div class="notes">
Toujours des définitions, aller vite
</div>


# Les bases

## Prérequis {data-background-image=img/pen-luis-llerena-16146.jpg}

---------------

### État d'esprit

- Rester calme et serin, être pro-actif
- Se poser les bonnes questions

. . .

- Ni défaitiste…

. . .

- …ni naïf

. . .

- Avoir peur des boîtes noires !

<div class="notes">
la confiance n'exclut pas le contrôle
</div>

------------------

### Compromis sécurité / facilité d’utilisation

- Oui, un bon mot de passe est plus long à taper
- Oui, c’est frustrant de taper 6 chiffres pour déverrouiller son téléphone

. . .

- Mais c’est le but

<div class="notes">
Le plus sûr des secrets est celui qu'on ne partage pas, plus vous sécuriserez
vos comportement plus dur sera l'accès et plus de barrières vous mettrez dans
vos communications.
</div>

------------------

### Se tenir au courant

- De l’actualité :
    - Next INpact
    - La Quadrature du Net
- Des solutions existantes :
    - Framasoft
- La sécurité numérique est une forme de course aux armements

<div class="notes">
Parler de veille, donner quelques sources grand public, rester calmes, la
quadrature, Next INpact

le concept de course aux armenents : ce que les services utilisaient il y a 20
ans est au mains des criminels depuis 15, des entreprises depuis 10 des
particuliers depuis 5 (ex : drones, bruteforce, ...)
</div>


## Des bonnes questions {data-background-image=img/pen-luis-llerena-16146.jpg}

--------------------

### Des bonnes questions

- **Q**uelles sont mes données ?
- **O**ù sont mes données ?
- **Q**ui a mes données ?
- **Q**uelles sont les menaces ?
- **Q**ue puis-je faire ?

------------------

### **Q**uelles sont mes données

- Tout ce dont vous êtes l'auteur
    - publication sur les réseaux sociaux
    - emails
    - listes d'achats
    - fichiers
- Tout ce qui vous concerne
    - contacts
    - détails de vos échanges (qui / quand / localisation / …)
    - orientation politique
    - préférences
    - comportements passés
- etc.

<div class="notes">
    données, metadonnées, interpretations des données, croisement des données
</div>

------------------

### **O**ù sont mes données

- Sur quel(s) appareil(s) ?
- Qui les traite ?
- Où cela se passe-t-il ?
- Sous quel régime juridique ?
- …

<div class="notes">
emplacement
régime de droit
vol
</div>

------------------

### **Q**ui a mes données

- Équipe d'administrateurs du service
- Opérateur de sauvegardes
- Sous-traitants
- Fournisseur d'accès
- Autorités du pays d'hébergement
- Publicitaires
- Lecteurs
- …

<div class="notes">
l'admin
le flic
le voleur
le public ...
</div>

------------------

### **Q**uelles sont les menaces ?

- Un État (présent… ou futur !)
- Une entreprise (GAFAM)
- Un patron
- Un assureur
- Un voisin
- …

<div class="notes">
Parler rapidement de méthode : pour se défendre, il faut connaitre ses
adversaires
</div>

------------------

#### États

- Loi renseignement
- Société panoptique
- Espionnage des estrangers du dehors
- Barbouses bien de chez nous
- État d'urgence permanent

<div class="notes">
Ici, ne pas trop insister, c'était le sujet de la conférence précédente,

- rappeler que rien n'oblige à déposer contre soi même
- on parlera des USA à la prochaine diapositive
- rappeler le secret postal et son histoire
</div>


------------------

#### États

<div class="notes">
- Obama != DJ Trump, un gouvernement peut basculer très vite dans un mode
    authoritaire. Par exemple : Etat d'urgence
</div>

![](img/GodEmperor.jpg)

------------------

#### GAFAM

![](img/stickerDiO_gafam.png)

------------------

#### GAFAM

- **G**oogle **A**pple **F**acebook **A**mazon **M**icrosoft

. . .

- Le *cloud* = l'ordinateur de quelqu'un autre

. . .

- Ils ont :
    - _vos_ emails
    - _votre_ carnet d'adresse
    - _vos_ amis
    - _vos_ positions politique
    - _vos_ secrets
    - _vos_ sauvegardes
    - _votre_ historique de navigation

. . .

(même le porn)

<div class="notes">
- Toujours bien faire l'analogie avec le courrier papier, et insister sur les
    budgets et les bases d'utilisateurs de ces monstres
    TODO : trouver des chiffres : A VALIDER
- Quelques chiffres et données :
    - Bénéfices d'Apple en 2015 comparables au PIB de nombreux états (même si la comparaison n'a qu'un sens limité)
      (numerama:http://www.numerama.com/business/148593-combien-dapple-valent-tous-les-pays-du-monde.html)
      Résultat Net 2016 : 45.687 milliards de dollars
    - En 2016, Google dépasse Apple en capitalisation boursière aux Etats Unis : 550 milliards de dollards dispersés au travers de différentes catégories d'actions (Wikipedia : Stratégies,classement BrandZ)
    - Facebook a acheté Instagram pour 1 milliard de dolars et WhatsApp pour 19 milliards de dollards (numerama : http://www.numerama.com/startup/facebook)
    - Facebook revendique 1.23 milliard d'utilisateurs quotidiens en Décembre 2016 (1.86 milliard mensuels) (source FB https://newsroom.fb.com/company-info/)


Profiter de cette slide pour introduire les "buzzwords" rappeler l'affaire
du cloud "souverain"
TODO : ilustration : A VALIDER (position (avant ou après ?), choix de l'illustration)
</div>

------------------

#### Criminels

- Prise des données en otage
- Vol d'appareils
- Profilage « catégoriel »
- Fausses nouvelles
- Arnaques diverses

. . .

- Lucratif !


<div class="notes">

dificile de donner des chiffres mais on estime que la cybercriminalité
classique (sans les revenus générés par le renseignement) génére plus de
revenus que le trafic de drogue, et ce depuis 2010 au moins (ZDnet, gemalto)

</div>

------------------

#### Curieux et autres nuisibles

- Publicitaire
- Patron
    - Enquête de bonnes mœurs
    - Loisirs
    - Lieu de vacances
    - Activités syndicales
- (ex)Partenaire trop curieux
- Type qui a trouvé votre smartphone dernier cri
- Fabriquant du machin « à commande vocale »

------------------

### Que puis je faire ?

- Rester calme
- Chercher des solutions :
    - politique
    - décentralisation
    - techniques (chiffrement, anonymisation…)

. . .

- **Penser aux autres** (s’il vous plaît)

<div class="notes">
ici : analyse, insister sur l'application de la méthodo

le message c'est peu importe le curseur où l'on place sa propre limite, il est
impératif de bien se protéger. De plus ne pas perdre de vue la "course" : les
moyens étatiques d'aujourd'hui seront les moyens des voyous de demain.

même si _vous_ n'avez rien à cacher il se peux que ceux autour de vous
n'aient pas ce confort, à ce titre vous pouvez devenir un vecteur d'attaque
dirigé vers autrui.
</div>



# Travaux pratiques

## **Fesses de bouc** {data-background-image=img/facebook.jpg}

------------------

### **Q**uelles sont mes données

**Données directes**

. . .

- Photos
- Posts
- Contacts et relations (sur plusieurs niveaux)
- Localisation
- Sites visités

-------------

**Données indirectes**

. . .

- Orientation politique
- Orientation sexuelle
- État relationnel
- Problèmes de santé
- Activités et engagements
- Emploi du temps

---------------

### **O**ù sont mes données

. . .

- Sur les serveurs de Facebook
- Notamment aux États-Unis

. . .

donc sous juridiction US

----------------

### **Q**ui a accès à mes données

. . .

- Facebook & ses employés
- Gouvernement US
- Publicitaires
- Pirates
- Mes amis
- Mes contacts

----------------

### **Q**uelles sont les menaces

. . .

- Publicité ciblée & intrusive
- Mon patron sait ce que je fais
- Mon assureur pourrait savoir mes problèmes de santé
- Mon gouvernement sait que je manifeste contre lui
- La police a mes photos de manifs
- Et je n’ai aucun contrôle dessus : potentiel *ad vitam*

------------------

### Que faire ?

. . .

- Verrouiller son compte
- Éviter FB et aller sur un autre réseau (ou pas)
- Réfléchir avant de publier
- Se déconnecter systématiquement
    - N’y aller qu’en navigation privée
    - Extensions associées
- Ne pas accepter n’importe qui (ton patron  ≠ ton ami)
- Pseudonyme et cloisonnement
- Mot de passe fort et unique
- Authentification à plusieurs facteurs

## **Gmail** {data-background-image="img/gmail.jpg"}

---------------------

### **Q**uelles sont mes données

. . .

- Contenu de la correspondance
- Correspondance
- Métadonnées (dates, localisation, contacts…)
- Achats en ligne
- État de mes finances
- Comptes associés et restauration des mots de passes des autres services
- Emploi du temps
- Google pay ?
- **Personnalité / vie privée**

-------------------------

### **O**ù sont mes données

. . .

- Sur les serveurs de Google
- Notamment aux US

. . .

donc sous juridiction US

--------------

### **Q**ui a mes données

. . .

- Google & ses employés
- Gouvernement US
- Publicitaires
- Pirates

--------------------

### **Q**uelles sont les menaces

. . .

- Publicité ciblée & intrusive
- Mon gouvernement sait que je manifeste contre lui
- La police a mon emploi du temps
- Espionnage industriel
- Vol d'identité
- Paiements…
- Revente de données

----------------------

### Solutions

. . .

- Verrouiller son compte
- Utiliser une autre boîte
- Chiffrement
- Se déconnecter systématiquement
- Utiliser la navigation privée /
- Utiliser des extensions adaptées
- Cloisonnement
- Mot de passe et unique
- Authentification à plusieurs facteurs

## **Smartphone** {data-background-image=img/smartphone.jpg}

-------------

### **Q**uelles sont mes données{.allowframebreaks}

**Données directes**

. . .

- Photos
- Posts
- Contacts, relations
- Appels
- Localisation
    - En temps réel
    - Historique

-----------

- Sites visités
- Applications
- Ambiance sonore
- Achats
- Recherches
- Comptes emails
- Comptes rs
- SMS
- Mots de passe

--------------

**Données indirectes**

. . .

- Orientation politique
- Problèmes de santé,
- Activités et engagements
- Planning
- Etc.

-----------------

**Plus grave**

. . .

- Authentification autres services (double authentification)
- Orientation sexuelle
- État relationnel
- Vos ami.e.s,
- Vos habitudes,
- Vos vices,
- Amant.e.s,
- Vos goûts
- …

---------------

### **O**ù sont mes données

. . .

- Sur mon téléphone (carte mémoire)
- Sur les serveurs Google / Apple
- Certaines peuvent être interceptées pendant leur trajet (SMS, appel…)
- Serveur de mon opérateur

-------------

### **Q**ui a accès à mes données

. . .

- Google / Apple & ses employés
- Gouvernement FR / US
- Publicitaires
- Pirates
- Mon opérateur
- Le pickpocket

-------------

### **Q**uelles sont les menaces

. . .

- Vol
- Vol d'argent
- Vol d'identité
- Espionnage
- Mon assureur pourrait savoir mes problèmes de santé
- Mon gouvernement sait que je manifeste contre lui
- La police a mes photos de manifs
- Écoute
- Perte de données (photos, contacts…)

-------------------

### Que faire

. . .

- Chiffrer
- Verrouiller son téléphone
- OS libres (Lineage, Ubuntu…)
- Verrouiller
- Cloisonnement : ne pas tout avoir sur son Smartphone
- Mot de passe et unique


## À vous de réfléchir pour…

. . .

- Votre PC portable
- Votre PC fixe
- Votre moteur de recherche
- Votre voiture
- Votre appareil photo
- Votre dropbox ou autre
- Votre boutique en ligne
- Votre fournisseur d’accès Internet (FAI)
- Votre hôtel
- Votre entreprise, votre école
- ...

----------------------------

- **Q**uelles sont mes données ?
- **O**ù sont mes données ?
- **Q**ui a mes données ?
- **Q**uelles sont les menaces ?
- **Q**ue puis-je faire ?

# Se défendre

---------------------

## Utiliser des logiciels libres

>  Logiciel libre désigne des logiciels qui respectent la liberté des
>  utilisateurs. En gros, cela veut dire que les utilisateurs ont la liberté
>  d'exécuter, copier, distribuer, étudier, modifier et améliorer ces logiciels.


<div class="notes">
pourquoi (confiance, audibilité, recommandation, éthique, niveau
technologique)
</div>


----------------------

## Mot de passe

. . .

et plus si nécessaire

-------------

### Qu'est-ce qu'un bon mot de passe

. . .

par exemple  `123456` est _très **mauvais**_

. . .

`Manolito49.3` est mieux mais reste dangereux

. . .

`M@vslR(c)Rsd1545_fessdébouc2`

<div class="notes">
À CASSANDRE
Mignonne, allons voir si la rose
Qui ce matin avait déclose
Sa robe de pourpre au soleil,
A point perdu cette vesprée,
Les plis de sa robe pourprée,
Et son teint au vôtre pareil.
Las ! voyez comme en peu d’espace,
Mignonne, elle a dessus la place
Las ! las ! ses beautés laissé choir !
Ô vraiment marâtre Nature,
Puis qu’une telle fleur ne dure
Que du matin jusques au soir !
Donc, si vous me croyez, mignonne,
Tandis que votre âge fleuronne
En sa plus verte nouveauté,
Cueillez, cueillez votre jeunesse :
Comme à cette fleur la vieillesse
Fera ternir votre beauté.
Ronsard (1524, Vendômois)
    Odes, I,17
</div>

---------------

### Gestion saine des mots de passe

- Un mot de passe différent par objet
    - Utiliser des suffixes ou préfixes
- Outils de gestion de mot de passe
- Authentification par moyens multiples

---------------

![](./img/password-checker.png)

-------------------

## Le chiffrement

### Kézako

> Le chiffrement est un procédé grâce auquel on souhaite rendre la compréhension
> d'un document impossible à toute personne qui n'a pas la clé de
> (dé)chiffrement. Ce principe est généralement lié au principe d'accès
> conditionnel.
. . .

> chiffrement asymétrique utilise des clés différentes : une paire composée
> d'une clé publique, servant au chiffrement, et d'une clé privée, servant à
> déchiffrer. Le point fondamental soutenant cette décomposition publique/privée
> est l'impossibilité de déduire la clé privée de la clé publique.

------------------

## Construire son coffre-fort numérique

### Principe

À l'aide d'un logiciel spécialisé (et libre) créer un espace chiffré
dans lequel mettre à l'abri ses secrets.

---------------


### Solution logicielle

**VeraCrypt** [lien](https://veracrypt.codeplex.com/)

> VeraCrypt est un logiciel utilitaire sous licence libre utilisé pour le
> chiffrement à la volée. Il est développé par la société française IDRIX et
> permet de créer un disque virtuel chiffré dans un fichier ou une partition.

<div class="notes">c'est un forck de truecrypt</div>


------------------------

### Les limites

- Le chiffrement se "voit" :
    - veracrypt est installé sur votre système
    - il y a **gros** fichier quelque part qui est illisible
- La loi vous impose de fournir le mot de passe
    - sur demande d'un juge
    - à une frontière
    - l'export de matériel chiffré est réglementé, renseignez vous
- Si vous perdez le mot de passe : tout est perdu
- Perte de performances

------------------

## Les services

<div class="notes">

TODO : dégoogliser internet, si c'est gratuit c'est vous le produits, signes de
qualité chez un fournisseur de services
</div>

- email
    - [Chatons](https://chatons.org/) - [GnuPG](https://www.gnupg.org/)
- chat
    - [OTR](https://otr.cypherpunks.ca/) - [tox](https://tox.chat/)
- recherche
    - [Qwant](http://www.qwant.com/) - [DuckDuckGo](https://duckduckgo.com/) - [StartPage/Ixquick](https://www.startpage.com)
- réseaux sociaux
    - [Diaspora](https://framasphere.org/)
- collaborer
    - [Framasoft](https://framaestro.org/)

---------------

## Vos appareils

------------------


### Ordinateur

- Système d'exploitation
    - [Debian](https://www.debian.org/) - [Fedora](https://getfedora.org/)
- Backups 3-2-1
- Coffre-fort chiffré
    - [Veracrypt](https://veracrypt.codeplex.com/)
- Gestion des mots de passe
    - [Keepass](http://keepass.info/)
- Navigateur(s)
    - [Firefox](https://www.mozilla.org/fr/firefox/new/)
    - [Https everywhere](https://www.eff.org/https-everywhere)
    - [Ublock Origin](https://github.com/gorhill/uBlock/)
    - [Lightbeam](https://www.mozilla.org/en-US/lightbeam/)
    - [Decentral eyes](https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/)

------------

- Chat / voix
    - [framatalk](https://framatalk.org/)
    - [Tox](https://tox.chat/)
- Pseudonymat / anonymat
    - [Tor](https://www.torproject.org/)

---------------------

### Portable

- Système d'exploitation
    - [Debian](https://www.debian.org/)
- Synchronisation
    - [Nextcloud](https://nextcloud.com/)
- Chiffrement du disque
    - [Veracrypt](https://veracrypt.codeplex.com/)
- VPN
- Webcam

-------------

### Smartphone

- Système d'exploitation
- Logithèque libre (F-Droid)
- Chiffrement complet
- Ne pas faire confiance :
    - Enlever la source électrique,
    - Laisser l'appareil au vestiaire
    - Pochette isolante
- Câbles

--------------------

### Stockages

- Chiffrement du support
    - [Veracrypt](https://veracrypt.codeplex.com/)
- Danger des "trouvailles"

--------------

- Déni probable

![](https://imgs.xkcd.com/comics/security.png)

# Questions

--------------

![cette présentation](img/url.png)

## Crédits Photos

- Illustrations pages de titres Unsplash ([Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/)) :
    - Définitions : Alex Read
    - Préquis : Luis Llerena
    - De bonnes questions : Luis Llerena
- Autres :
    - GAFAM : Stickers Framasoft : Creative Commons By-SA 4.0 ( Simon "Gee" Giraudot)

<div class="notes">

TODO : Confirmer besoin crédits photos
TODO : Compléter
</div>


# Pour aller plus loin

------------------

## Approfondir

---------------

### Rapide introduction au chiffrement asymétrique

> Chiffrement asymétrique utilise des clés différentes : une paire composée d'une clé publique, servant au chiffrement, et d'une clé privée, servant à déchiffrer. Le point fondamental soutenant cette décomposition publique/privée est l'impossibilité de déduire la clé privée de la clé publique.

--------------

![](img/Alice+bob_step_1.svg)

Alice et Bob souhaitent communiquer,

1. Ils génèrent chacun une paire de clefs
    - Une publique (en vert) qu'ils échangent et distribuent autour d'eux
    - Une privée (en rouge) à conserver en secret et précieusement

-------------------

![](img/Alice+bob_step_2.svg)

2. Bob chiffre le message pour Alice avec la sa clef privée (en vert)
3. _Seule_ Alice peut déchiffrer le message avec sa clef privée (en rouge)

---------------------------

## Sources d'information

--------------------------

### Les associations, la presse "tout public"

- [la Quadature du net](https://www.laquadrature.net/) et surtout son [wiki](https://wiki.laquadrature.net/)
- [L'April](https://www.april.org/) association de promotion du logiciel libre en France
- [Framasoft](https://framasoft.org/) pour son [annuaire](https://framasoft.org/rubrique2.html) et surtout sont excellent [blog](https://framablog.org/)
- [NextINpact](https://www.nextinpact.com/) très bon site d'information en ligne
- [The Intercept](https://theintercept.com/) Le média fondé par [Glenn Greenwald](https://fr.wikipedia.org/wiki/Glenn_Greenwald)

--------------

### Les institutions

- [La CNIL](https://www.cnil.fr) Commission Nationale Informatique & Libertés
- [L'ANSSI](https://www.ssi.gouv.fr/) Agence Nationale de la Sécurité des Systèmes d'information


-------------------

### Sources d'information et guides pratiques

- Guide d'hygiène informatique de l'ANSSI [Pdf](https://www.ssi.gouv.fr/uploads/2017/01/guide_hygiene_informatique_anssi.pdf)
- [The Privacy Enthusiast's Guide to Using Android](http://lifehacker.com/the-privacy-enthusiasts-guide-to-using-android-1792432725) (lifehacker, en anglais)
- [Le Kit du CypherPunk ou CryptoAnarchiste](https://steemit.com/fr/@thierryb/le-kit-du-cypherpunk-ou-cryptoanarchiste)


-------------------

## Quelques pistes de solutions pour bien commencer

### Systèmes d'exploitation libres et "propres"

- [Debian](https://debian.org) et sa dérivée grand public [Ubuntu](https://ubuntu.com)
- [Fedora](https://getfedora.org/)
- [lineage os](https://lineageos.org/) est une version libre d'android

-------


### Chiffrement

- La [Fiche](https://www.cnil.fr/fr/comment-chiffrer-ses-documents-et-ses-repertoires) de la CNIL sur le chiffrement.
- [Gnu Prvacy guard](https://gnupg.org/) chiffrement des emails
- [Veracrypt](https://veracrypt.codeplex.com/) pour les systèmes de fichiers
- les conseils de la [CNIL](https://www.cnil.fr/) sur les mots de passe : [lien](https://www.cnil.fr/fr/journee-de-la-protection-des-donnees-les-mots-de-passe-nauront-plus-de-secret-pour-vous)
- [Tor Browser](https://www.torproject.org/) navigateur anonyme in-traçable


------------

### Mots de passe

- les conseils de la CNIL sur les mots de passe : [lien](https://www.cnil.fr/fr/journee-de-la-protection-des-donnees-les-mots-de-passe-nauront-plus-de-secret-pour-vous)
- [Keepass](https://keepass.info/) et [KeepassX](https://www.keepassx.org/) gestionnaires de mots de passe


------------

### Configurer son navigateur

- [Mozzila Firefox](https://www.mozilla.org/) Navigateur libre & moderne
- [https everywhere](https://www.eff.org/https-everywhere) l'extetion de  l'[EFF](https://www.eff.org/)
- [disable webrtc](https://github.com/ChrisAntaki/disable-webrtc-firefox) et [Noscript](https://noscript.net/) pour mieux maitriser ce qui s'exécute sur votre navigateur
- [mailvelope](https://www.mailvelope.com/) pour le chiffrement sur webmail
- [uBlock Origin](https://github.com/gorhill/uBlock) contre les contenus intrusifs

------------

### Auto-Hébergement

<div class="notes">
Pour aller plus loin, l'auto-hébergement consiste à héberger ses services soi-même. On peut installer ses serveurs from scratch en installant tout "manuellement" ou en utilisant des solutions plus clef en main dont voici quelques exemples...
</div>

Pour héberger soi-même ses services, ses données...

_Nécessite une attention particulière (sécurité, sauvegarde...)_

- [Yunohost](https://yunohost.org) : Solution serveur d'hébergement simplifié de services Internet
- [Cozy](https://cozy.io) : Solution clef en main de serveur type "cloud" personnel
- ...
- ou sur mesure tout "à la main" (pour les utilisateurs avancés)...


------------

### Autres

- [Edward Snowden](https://twitter.com/snowden) à propos des [mots de passe](https://www.youtube.com/watch?v=yzGzB-yYKcc)
- [Le projet GNU](https://gnu.org)
- [Le blog De Jean-Marc Manhack](http://bugbrother.blog.lemonde.fr/) et son [Compte twitter](https://twitter.com/manhack)

---------------

![](img/gnu.jpg)


----------------

### Histoires d'horreur (vraies)

- [Le "cas" Brandon Mayfield](https://framablog.org/2014/02/12/le-cas-brandon-mayfield/)
- [L'histoire de Michele Catalano](https://www.theguardian.com/world/2013/aug/01/new-york-police-terrorism-pressure-cooker)
